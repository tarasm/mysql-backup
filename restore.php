<?php

header("Content-Type: text/plain; Charset=UTF-8");

error_reporting(E_ALL ^ E_NOTICE);
include_once(dirname (__FILE__) . "/mysqldump.class.php");

$mysqldump = false;
include_once(dirname (__FILE__) . "/../configuration.php");
if(class_exists("JConfig")){
  print "Joomla\n";
  $config = new JConfig();
  $mysqldump = new MySQLDump( $config->host,
    $config->user, $config->password,
    $config->db );
  $mysqldump->use_prefix = $config->dbprefix;
}
else{
  include_once(dirname (__FILE__) . "/../config.php");
  if(defined("DB_PREFIX") && defined('DB_HOSTNAME') && defined('DB_USERNAME') &&
      defined('DB_PASSWORD') && defined('DB_DATABASE')){
    $mysqldump = new MySQLDump(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
    $mysqldump->use_prefix = DB_PREFIX;
  } else if(is_file(dirname (__FILE__) . "/../application/config/database.php")){
    include_once(dirname (__FILE__) . "/../application/config/database.php");

    $mysqldump = new MySQLDump( $db[$active_group]['hostname'],
                                $db[$active_group]['username'], $db[$active_group]['password'],
                                $db[$active_group]['database'] );

    $mysqldump->use_prefix = $db[$active_group]['dbprefix'];
  }
  else{

//    $mysqldump = new MySQLDump("localhost",  "root", "", "database");
 }

}

if(!$mysqldump){ die( "No configuration"); };

print "\n\nRestore\n\n";
$mysqldump->setPath(dirname(__FILE__) . "/data" );
$mysqldump->restore();


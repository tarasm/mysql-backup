<?php

if(isset($_POST["action"])){

  if($_POST["debug"]){ header("Content-type: text/plain"); }
  else{ ob_start(); };

  include_once(dirname(__FILE__) . "/init.php");
  
  if($_POST["action"] == "dump"){

     $mysqldump->setPath(dirname(__FILE__) . "/data" );
//$mysqldump->dumpTable('inform_pages');
//$mysqldump->dumpTable('inform_pages_spec');
     $mysqldump->dumpTable();
  } else if($_POST["action"] == "restore"){

     $mysqldump->setPath(dirname(__FILE__) . "/data" );
     $mysqldump->restore();

  };

  if(!$_POST["debug"]){ /* header("Location: "); */ }
  else{ exit; };

  ob_end_flush();
//  ob_clean();

} else {
header("Content-Type: text/html; Charset=UTF-8");
?><html>
  <head>
    <style>
      input { margin: 5px; }
    </style>

  </head>
  <body>

    <form action="" method="POST">
      <label><input type="checkbox" name="debug" value="1" /> Debug</label>
      <input type="submit" value="dump" name="action" />
      <input type="submit" value="restore" name="action" />
    </form>
  </body>
</html><?php

};

<?php
/**
  * Backup and restore MySQL database
  * see dump.php and restore.php
  * Autor Taras <taras.m0@yandex.ru>
  */
class MySQLDump {

  public $path;
  public $use_prefix;
  protected $mysql_link;

  function __construct($server="", $username="", $password="",
                       $database="", $charset="utf8",$pcon=false) {
    if ($pcon) {
      $this->mysql_link = @mysql_pconnect($server,$username,$password);
    } else {
      $this->mysql_link = @mysql_connect ($server,$username,$password);
    };

    if(!$this->mysql_link)
      throw new MySQLDumpException("Cannot connect MySQL server: " . $server);

    if(!mysql_select_db($database,$this->mysql_link))
        throw new MySQLDumpException("Cannot select database: " . $database);

    foreach(array("SET NAMES '{$charset}'",
                  "SET CHARACTER SET '{$charset}'",
                  "SET time_zone = '+00:00'") as $query){
       if(!mysql_query($query, $this->mysql_link))
            throw new MySQLDumpException("Cannot " . $query);
    };
  }

  function  __destruct(){
    @mysql_close($this->mysql_link);
  }

  function getTables(){
    $ret = array();
    if($result = mysql_query("SHOW TABLES")){
      while($table = mysql_fetch_array($result)){
        $ret[] = $table[0];  };
    }else{ throw new MySQLDumpException("Cannot get tables list"); };
    return $ret;
  }

  function dumpTable($name = false){
    foreach( ($name ? array($name) : $this->getTables()) as $table){
      $table = new table_dumped($this, $table);
      $table->dumpHeader();
      $table->dumpData();
 //     break;
    };
  }

  public function setPath($path = false){
    if(!$path){
      $path = dirname(__FILE__) . DIRECTORY_SEPARATOR . date("YmdHis");
    }

    if(!is_dir($path)) mkdir($path,0777);
    $this->path = $path;
  }

  public function restore($path = false){
    if(!$path) $path = $this->path;

    $head = array(); $data = array();
    foreach(scandir($path) as $file){
      if(!is_file($path . DIRECTORY_SEPARATOR . $file)) continue;

      if(preg_match("/(.*?)\\.head$/i",$file,$matches)){
        if(in_array(($path . DIRECTORY_SEPARATOR . $matches[1]), $data)){
                array_splice($data,
                   array_search(($path . DIRECTORY_SEPARATOR . $matches[1]),$data,TRUE),1);
        };

        $head[] = $path . DIRECTORY_SEPARATOR . $matches[1];

      }
      elseif(preg_match("/(.*?)\\.data$/i",$file,$matches)){
        if(!in_array(($path . DIRECTORY_SEPARATOR . $matches[1]), $head)){
                  $data[] = $path . DIRECTORY_SEPARATOR . $matches[1]; };
      };
    }
    var_dump( $head,$data);

// Restore other table data
    foreach($data as $data_file){
      $this->restore_data_table($data_file . ".data");
    }


    $drop = array(); $create = array();
    foreach($head as $head_file){

       foreach(preg_split("/\\;[\\r\\n]+/",
            $this->replacePrefix(file_get_contents($head_file . ".head"),
                            "#__",$this->use_prefix )) as  $query_head){

           if(preg_match("/^DROP\\s+TABLE/i",$query_head)){
              $drop[] = $query_head; }
           else if(preg_match("/^CREATE\\s+TABLE/i",$query_head)){
              $create[] = array("query" => $query_head, "file" => $head_file); }
           else{
                if(mysql_query($query_head)){
print "$query_head\n";
                };
           };
       };
    };

// DROP Table
    while(count($drop) > 0){
      $apply = false;

      foreach($drop as $key => $query_head){   
        if(mysql_query($query_head)){
print "$query_head\n";
           $apply = true;  array_splice($drop, $key, 1);
           break;
        };
      };

      if(!$apply){ throw new MySQLDumpException("Cannot DROP Table SQL='" . $drop[0] ."'"); };
    };


// Restore table structure and data

    while(count($create) > 0){
      $apply = false;

      foreach($create as $key => $query_head){   
        if(mysql_query($query_head["query"])){
print $query_head["query"] . "\n";
           $apply = true;
           $this->restore_data_table($query_head["file"] . ".data");

           array_splice($create, $key, 1);
           break;
        };
      };

      if(!$apply){ throw new MySQLDumpException("Cannot Restore Table SQL='" . $create[0]["query"] ."'"); };
    };

  }

  private function restore_data_table($data_file = false){
    if(!$data_file){ return false; };
    if(!is_file($data_file)){ return false; };

      if(!($f=fopen($data_file,"r")))
        throw new MySQLDumpException("Cannot open data file: " . $data_file);

      while( ! feof($f)){
//        print $this->replacePrefix(str_replace("\r","",str_replace("\n","",
//                         fgets($f))),
//                 "#__",$this->use_prefix ) . "\n";
        mysql_query( $this->replacePrefix(str_replace("\r","",str_replace("\n","",
                         fgets($f))),
                   "#__",$this->use_prefix ));

        set_time_limit(30);
      };
    

  }


  public function replacePrefix($sql, $prefix_old, $prefix_new){
    // Initialize variables.
    $escaped = false;
    $startPos = 0;
    $quoteChar = '';
    $literal = '';

    $sql = trim($sql);
    $n = strlen($sql);

    while ($startPos < $n)
    {
      $ip = strpos($sql, $prefix_old, $startPos);
      if ($ip === false)
      {
        break;
      }

      $j = strpos($sql, "'", $startPos);
      $k = strpos($sql, '"', $startPos);
      if (($k !== false) && (($k < $j) || ($j === false)))
      {
        $quoteChar = '"';
        $j = $k;
      }
      else
      {
        $quoteChar = "'";
      }

      if ($j === false)
      {
        $j = $n;
      }

      $literal .= str_replace($prefix_old, $prefix_new, substr($sql, $startPos, $j - $startPos));
      $startPos = $j;

      $j = $startPos + 1;

      if ($j >= $n)
      {
        break;
      }

      // quote comes first, find end of quote
      while (true)
      {
        $k = strpos($sql, $quoteChar, $j);
        $escaped = false;
        if ($k === false)
        {
          break;
        }
        $l = $k - 1;
        while ($l >= 0 && $sql{$l} == '\\')
        {
          $l--;
          $escaped = !$escaped;
        }
        if ($escaped)
        {
          $j = $k + 1;
          continue;
        }
        break;
      }
      if ($k === false)
      {
        // error in the query - no end quote; ignore it
        break;
      }
      $literal .= substr($sql, $startPos, $k - $startPos + 1);
      $startPos = $k + 1;
    }
    if ($startPos < $n)
    {
      $literal .= substr($sql, $startPos, $n - $startPos);
    }

    return $literal;
  }

}

class table_dumped{
  private $name;
  private $MySQL;
  private $path;

  function __construct($MySQL,$name){
    $this->MySQL = $MySQL;
    $this->name = $name;

    if(($this->MySQL->path ) &&
        (is_dir($this->MySQL->path))){
      $this->path = $this->MySQL->path;
    }else{
      $this->path = dirname(__FILE__);
    }
  }


  function dumpHeader($fileName = false){
    if(!$fileName) $fileName = $this->MySQL->replacePrefix( $this->name,
      $this->MySQL->use_prefix,"_tab_") . ".head";
    file_put_contents($this->path . DIRECTORY_SEPARATOR . $fileName ,
        "DROP TABLE IF EXISTS `" . $this->MySQL->replacePrefix( $this->name,
          $this->MySQL->use_prefix,"#__") . "`;\n" .
      $this->head());

//    var_dump($this->data());
  }

  function dumpData($fileName = false){
    if(!$fileName) $fileName = $this->MySQL->replacePrefix( $this->name,
          $this->MySQL->use_prefix,"_tab_") . ".data";

    if(!($f = fopen($this->path . DIRECTORY_SEPARATOR . $fileName,"w")))
          throw new MySQLDumpException("Cannot open: " . $fileName);


    $head = "INSERT INTO `" . $this->MySQL->replacePrefix( $this->name,
                $this->MySQL->use_prefix,"#__") . "` (" . implode(",",array_map(
           "add_field_quote",
           $this->getFields()   )) . ") VALUES ";


    $result = mysql_unbuffered_query("SELECT * FROM `" . $this->name);
    while($row = mysql_fetch_row($result)){
      fwrite($f,$head . "(" . implode(",",array_map(
                   "add_escape_quote",$row)) . ");\n" );

      set_time_limit(30);
    };
    fclose($f);
    mysql_free_result($result);
  }

  function  getFields(){
    $ret = array();
    if(!($result = mysql_query("SHOW FIELDS FROM `" . $this->name . '`')))
               throw new MySQLDumpException("Cannot get fields list");


    while($field = mysql_fetch_assoc($result)){
      $ret[] = $field["Field"];
    };
    return $ret;
  }

  function head(){
    if(!($result = mysql_query("SHOW CREATE TABLE `" . $this->name . "`")))
        throw new MySQLDumpException("Cannot get head for table: " . $this->name);

    $table = mysql_fetch_assoc($result);
    return $this->MySQL->replacePrefix( $table["Create Table"],
      $this->MySQL->use_prefix,"#__");
    }
}

function add_field_quote($field){ return "`" . $field . "`"; };
function add_escape_quote($field){
  return "'" . str_replace("\r","\\r",
    str_replace("\n","\\n",
      mysql_real_escape_string($field))) . "'";
};

class MySQLDumpException extends Exception{}